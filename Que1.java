//Program to find sum of all digits of a number

package assignNo1;

import java.util.Scanner;

public class Que1 {

	public static void main(String[] args) 
	{
		int num,sum=0,x1,x2,x3;
		System.out.println("Enter a number between 0 to 1000");
		Scanner sc=new Scanner(System.in);
		num=sc.nextInt();
		  while(num != 0) {
	           sum = sum + (num % 10);
	           num = num /10;
		  }
		  System.out.println("Sum of number ="+sum);
		/*  
		if(num>0 && num<1000)
		{
			x1=num%10;
			num=num/10;
			x2=num%10;
			num=num/10;
			x3=num%10;
			sum=x1+x2+x3;
			System.out.println("Sum of number ="+sum);
		}
		else
			System.out.println("Entered number does not belong between 0 to 1000");
		*/
	}

}
